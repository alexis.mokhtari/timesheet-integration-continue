package tn.esprit.spring.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import tn.esprit.spring.entities.Contrat;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Entreprise;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.entities.Timesheet;
import tn.esprit.spring.repository.ContratRepository;
import tn.esprit.spring.repository.DepartementRepository;
import tn.esprit.spring.repository.EmployeRepository;
import tn.esprit.spring.repository.TimesheetRepository;

@Service
public class EmployeServiceImpl implements IEmployeService {
    
	
	
	@Autowired
	EmployeRepository employeRepository;
	@Autowired
	DepartementRepository deptRepository;
	@Autowired
	ContratRepository contratRepoistory;
	@Autowired
	TimesheetRepository timesheetRepository;
	
	private static final Logger l = LogManager.getLogger(EmployeServiceImpl.class);

	@Override
	public Employe authenticate(String login, String password) {
		l.info("In authenticate : login =" + login);
		 
		return employeRepository.getEmployeByEmailAndPassword(login, password);
	
	   }

	@Override
	public int addOrUpdateEmploye(Employe employe) {
		l.info("In addOrUpdate : " + employe);
		employeRepository.save(employe);
		l.info("out of addOrUpdate:" + employe.getId());
		return employe.getId();
	}


	public void mettreAjourEmailByEmployeId(String email, int employeId) {
		l.info("In getEmployeById : " + employeId);
		Employe employe = employeRepository.findById(employeId).get();
		l.info("In added new Email : " + email);
		employe.setEmail(email);
		l.info("employe updated : " + employe);
		employeRepository.save(employe);
      
	
	}

	@Transactional	
	public void affecterEmployeADepartement(int employeId, int depId) {
		try {

			l.info(" affecter employe a departement");
			l.debug("Employe ID : %d", employeId);
			l.debug("Department ID : %d", depId);

			Optional<Departement> fdep = deptRepository.findById(depId);
			Optional<Employe> femp = employeRepository.findById(employeId);
		
			if (fdep.isPresent()&&femp.isPresent()) 
			{
				Departement dep = fdep.get();
				Employe empl = femp.get();
				l.info(empl.getNom()+"entre au departement : " + dep.getName());
					dep.getEmployes().add(empl);
			}
				
			
			else {
				l.warn("Cannot find department or the employe ");
			}
			}
			catch (Exception e) { l.error("Erreur dans AffetecterEmploYerDepart() : " + e); }
		
	

	}
	
	
	
	
	
	@Transactional
	public void desaffecterEmployeDuDepartement(int employeId, int depId)
	{
		try {

			l.info(" desaffecter employe du departement");
			l.debug("Employe ID : %d", employeId);
			l.debug("Department ID : %d", depId);
			Optional<Departement> fdep = deptRepository.findById(depId);
			int t = 0;
			if (fdep.isPresent()) 
			{
				Departement dep = fdep.get();
			int employeNb = dep.getEmployes().size();
			for(int index = 0; index < employeNb; index++)
				{
				if(dep.getEmployes().get(index).getId() == employeId)
					{
					l.info(dep.getEmployes().get(index).getPrenom()+"Ã  quitter le departement : " + dep.getName());

					dep.getEmployes().remove(index);
					t = 1;
					break;
					}
				}
			}
			else {
				l.warn("Cannot find department with ID : %d", depId);
			}
			if(t==0)
			{
				l.warn("Cannot find employe id : %d in department with ID : %d",employeId, depId);
			}
			}
			catch (Exception e) { l.error("Erreur dans DesaffecterEmployeDepart : " + e); }
		
	} 
	
	// Tablesapce (espace disque) 

	public int ajouterContrat(Contrat contrat) {
		contratRepoistory.save(contrat);
		return contrat.getReference();
	}

	public void affecterContratAEmploye(int contratId, int employeId) {
		Contrat contratManagedEntity = contratRepoistory.findById(contratId).get();
		Employe employeManagedEntity = employeRepository.findById(employeId).get();

		contratManagedEntity.setEmploye(employeManagedEntity);
		contratRepoistory.save(contratManagedEntity);

	}

	public String getEmployePrenomById(int employeId) {
		Employe employeManagedEntity = employeRepository.findById(employeId).get();
		return employeManagedEntity.getPrenom();
	}
	 
	public void deleteEmployeById(int employeId)
	{
		try {

			l.info(" effacer employe");
			l.debug("Employe ID : %d", employeId);
			Optional<Employe> temploye = employeRepository.findById(employeId);
			if(temploye.isPresent())
			{	
				Employe employe = temploye.get();
			for(Departement dep : employe.getDepartements()){
				dep.getEmployes().remove(employe);
			}

			employeRepository.delete(employe);
			l.info(employe.getNom()+"effacer ");
			}
			else {
				l.warn("Cannot find employe");
			}
			}
			catch (Exception e) { l.error("Erreur dans DeleteEmploye() : " + e); }
		
	}

	public void deleteContratById(int contratId) {
		Contrat contratManagedEntity = contratRepoistory.findById(contratId).get();
		contratRepoistory.delete(contratManagedEntity);

	}

	public int getNombreEmployeJPQL() {
		return employeRepository.countemp();
	}

	public List<String> getAllEmployeNamesJPQL() {
		return employeRepository.employeNames();

	}

	public List<Employe> getAllEmployeByEntreprise(Entreprise entreprise) {
		return employeRepository.getAllEmployeByEntreprisec(entreprise);
	}

	public void mettreAjourEmailByEmployeIdJPQL(String email, int employeId) {
		employeRepository.mettreAjourEmailByEmployeIdJPQL(email, employeId);

	}
	public void deleteAllContratJPQL() {
		employeRepository.deleteAllContratJPQL();
	}

	public float getSalaireByEmployeIdJPQL(int employeId) {
		return employeRepository.getSalaireByEmployeIdJPQL(employeId);
	}

	public Double getSalaireMoyenByDepartementId(int departementId) {
		return employeRepository.getSalaireMoyenByDepartementId(departementId);
	}

	public List<Timesheet> getTimesheetsByMissionAndDate(Employe employe, Mission mission, Date dateDebut,
			Date dateFin) {
		return timesheetRepository.getTimesheetsByMissionAndDate(employe, mission, dateDebut, dateFin);
	}

	public List<Employe> getAllEmployes() {
		l.info(employeRepository.findAll());
		return (List<Employe>) employeRepository.findAll();
		
	}

}
